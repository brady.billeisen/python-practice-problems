# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(single_list):
    list1 = []
    list2 = []
    if len(single_list) % 2 == 0:
        len(single_list) / 2
        for index, digit in enumerate(single_list, start=1):
            if index <= len(single_list) / 2:
                list1.append(digit)
            elif index > len(single_list) / 2:
                list2.append(digit)
        return list1, list2


print(halve_the_list([1, 2, 3, 4]))
print(halve_the_list([2, 4, 6, 8]))
print(halve_the_list([2, 1, 5, 3, 9, 4, 8, 4, 6, 4]))
