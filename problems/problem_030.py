# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    if len(values) <= 1:
        return None
    highest = values[0]
    second_highest = values[0]
    for value in values:
        if value > highest:
            second_highest = highest
            highest = value
        elif value > second_highest:
            second_highest = value
    return second_highest


print(find_second_largest([1, 2, 3, 4]))
print(find_second_largest([2, 4, 6, 8]))
print(find_second_largest([3, 6, 9, 12]))
print(find_second_largest([4, 8, 12, 16]))
