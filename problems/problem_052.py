# Write a function that meets these requirements.
#
# Name:       generate_lottery_numbers
# Parameters: none
# Returns:    a list of six random unique numbers
#             between 1 and 40, inclusive
#
# Example bad results:
#    [4, 2, 3, 3, 1, 5] duplicate numbers
#    [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
from random import randint


def generate_lottery_numbers():
    unique_nums = []
    while len(unique_nums) < 6:
        unique_num = randint(1, 40)
        if unique_num not in unique_nums:
            unique_nums.append(unique_num)
    return unique_nums


print(generate_lottery_numbers())
