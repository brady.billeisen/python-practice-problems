# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


print(can_make_pasta(["flour", "eggs", "oil"]))
print(can_make_pasta(["yo", "mama", "sucks"]))
print(can_make_pasta(["blah", "blah", "blah"]))
print(can_make_pasta(["flour", "milk", "oil"]))
