# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if len(values) == 0:
        return None
    sum = 0
    for value in values:
        sum += value
    return sum


print(calculate_sum([1, 2, 3, 4]))
print(calculate_sum([2, 4, 6, 8]))
print(calculate_sum([7, 3, 5, 8]))
print(calculate_sum([10, 10, 10, 3]))
