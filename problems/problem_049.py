# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7
def sum_two_numbers(num1, num2):
    return num1 + num2


print(sum_two_numbers(15, 8))
print(sum_two_numbers(23, 10))
print(sum_two_numbers(13, 30))
print(sum_two_numbers(41, 16))
print(sum_two_numbers(3, 14))
