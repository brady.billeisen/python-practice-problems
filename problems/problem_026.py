# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    if len(values) == 0:
        return None
    total_grades = 0
    for grade in values:
        total_grades += grade
        average = total_grades / len(values)
    if average >= 90:
        return "A"
    elif average < 90 and average >= 80:
        return "B"
    elif average < 80 and average >= 70:
        return "C"
    elif average < 70 and average >= 60:
        return "D"
    else:
        return "F"


print(calculate_grade([95, 91, 89, 90]))
print(calculate_grade([40, 50, 65, 80]))
print(calculate_grade([71, 78, 73, 74]))
print(calculate_grade([63, 67, 69, 61]))
print(calculate_grade([]))
