# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you


print(calculate_average([1, 2, 3, 4]))
print(calculate_average([2, 4, 6, 8]))
print(calculate_average([]))
print(calculate_average([10, 23, -27, 15]))
