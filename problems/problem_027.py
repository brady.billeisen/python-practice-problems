# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    max = 0
    for value in values:
        if value > max:
            max = value
    return max


print(max_in_list([1, 2, 3, 4]))
print(max_in_list([23, 17, 11, 7]))
print(max_in_list([33, 45, 73, 29]))
print(max_in_list([2, 4, 6, 8]))
print(max_in_list([]))
